msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:50+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:1
msgid "How to use clone layers."
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Linked Clone"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:15
msgid "Clone Layer"
msgstr "Cloner le calque"

#: ../../reference_manual/layers_and_masks/clone_layers.rst:20
msgid "Clone Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:22
msgid ""
"A clone layer is a layer that keeps an up-to-date copy of another layer. You "
"cannot draw or paint on it directly, but it can be used to create effects by "
"applying different types of layers and masks (e.g. filter layers or masks)."
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:25
msgid "Example uses of Clone Layers."
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:27
msgid ""
"For example, if you were painting a picture of some magic person and wanted "
"to create a glow around them that was updated as you updated your character, "
"you could:"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:29
msgid "Have a Paint Layer where you draw your character"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:30
msgid ""
"Use the Clone Layer feature to create a clone of the layer that you drew "
"your character on"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:31
msgid ""
"Apply an HSV filter mask to the clone layer to make the shapes on it white "
"(or blue, or green etc.)"
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:32
msgid "Apply a blur filter mask to the clone layer so it looks like a \"glow\""
msgstr ""

#: ../../reference_manual/layers_and_masks/clone_layers.rst:34
msgid ""
"As you keep painting and adding details, erasing on the first layer, Krita "
"will automatically update the clone layer, making your \"glow\" apply to "
"every change you make."
msgstr ""
