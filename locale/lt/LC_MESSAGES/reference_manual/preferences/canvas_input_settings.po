# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 03:36+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<generated>:1
msgid "Profile"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:1
msgid "Canvas input settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
#: ../../reference_manual/preferences/canvas_input_settings.rst:16
msgid "Canvas Input Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:11
msgid "Tablet"
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:18
msgid ""
"Krita has ways to set mouse and keyboard combinations for different actions. "
"The user can set which combinations to use for a certain Krita command over "
"here. This section is under development and will include more options in "
"future."
msgstr ""

#: ../../reference_manual/preferences/canvas_input_settings.rst:21
msgid "The user can make different profiles of combinations and save them."
msgstr ""
