# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:24+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/filters/Lens-blur-filter.png"
msgstr ".. image:: images/filters/Lens-blur-filter.png"

#: ../../reference_manual/filters/blur.rst:1
msgid "Overview of the blur filters."
msgstr "Översikt av oskärpefilter."

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:15
#: ../../reference_manual/filters/blur.rst:39
msgid "Blur"
msgstr "Oskärpa"

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:25
msgid "Gaussian Blur"
msgstr "Gaussisk oskärpa"

#: ../../reference_manual/filters/blur.rst:10
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/filters/blur.rst:17
msgid ""
"The blur filters are used to smoothen out the hard edges and details in the "
"images. The resulting image is blurry. below is an example of a blurred "
"image. The image of Kiki on right is the result of blur filter applied to "
"the image on left."
msgstr ""
"Oskärpefiltren används för att jämna ut skarpa kanter och detaljer på "
"bilder. Den resulterande bilden är suddig. Nedan visas ett exempel på en "
"suddig bild. Bilden av Kiki till höger är resultatet av att använda ett "
"oskärpefilter på bilden till vänster."

#: ../../reference_manual/filters/blur.rst:21
msgid ".. image:: images/filters/Blur.png"
msgstr ".. image:: images/filters/Blur.png"

#: ../../reference_manual/filters/blur.rst:22
msgid "There are many different filters for blurring:"
msgstr "Det finns många olika filter för oskärpa:"

#: ../../reference_manual/filters/blur.rst:27
msgid ""
"You can input the horizontal and vertical radius for the amount of blurring "
"here."
msgstr "Horisontell och vertikal radie för oskärpevärdet kan matas in här."

#: ../../reference_manual/filters/blur.rst:30
msgid ".. image:: images/filters/Gaussian-blur.png"
msgstr ".. image:: images/filters/Gaussian-blur.png"

#: ../../reference_manual/filters/blur.rst:32
msgid "Motion Blur"
msgstr "Rörelseoskärpa"

#: ../../reference_manual/filters/blur.rst:34
msgid ""
"Doesn't only blur, but also subtly smudge an image into a direction of the "
"specified angle thus giving a feel of motion to the image. This filter is "
"often used to create effects of fast moving objects."
msgstr ""
"Lägger inte bara till oskärpa, men smetar också ut en bild hårfint i den "
"angivna vinkelns riktningen, och ger på så sätt en känsla av rörelse i "
"bilden. Filtret används ofta för att skapa effekten att ett objekt rör sig "
"snabbt."

#: ../../reference_manual/filters/blur.rst:37
msgid ".. image:: images/filters/Motion-blur.png"
msgstr ".. image:: images/filters/Motion-blur.png"

#: ../../reference_manual/filters/blur.rst:41
msgid "This filter creates a regular blur."
msgstr "Filtret skapar en vanlig oskärpa."

#: ../../reference_manual/filters/blur.rst:44
msgid ".. image:: images/filters/Blur-filter.png"
msgstr ".. image:: images/filters/Blur-filter.png"

#: ../../reference_manual/filters/blur.rst:46
msgid "Lens Blur"
msgstr "Linsoskärpa"

#: ../../reference_manual/filters/blur.rst:48
msgid "Lens Blur Algorithm."
msgstr "Algoritm för linsoskärpa."
