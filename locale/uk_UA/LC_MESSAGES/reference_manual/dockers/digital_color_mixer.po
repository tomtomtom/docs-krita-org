# Translation of docs_krita_org_reference_manual___dockers___digital_color_mixer.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___digital_color_mixer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 09:42+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/dockers/digital_color_mixer.rst:1
msgid "Overview of the digital color mixer docker."
msgstr "Огляд бічної панелі змішування цифрових кольорів."

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
#: ../../reference_manual/dockers/digital_color_mixer.rst:16
msgid "Digital Color Mixer"
msgstr "Змішування цифрових кольорів"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Mixing"
msgstr "Змішування кольорів"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Selector"
msgstr "Засіб вибору кольору"

#: ../../reference_manual/dockers/digital_color_mixer.rst:19
msgid ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"
msgstr ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"

#: ../../reference_manual/dockers/digital_color_mixer.rst:20
msgid "This docker allows you to do simple mathematical color mixing."
msgstr ""
"За допомогою цієї бічної панелі ви можете виконувати просте математичне "
"змішування кольорів."

#: ../../reference_manual/dockers/digital_color_mixer.rst:22
msgid "It works as follows:"
msgstr "Ось, як вона працює:"

#: ../../reference_manual/dockers/digital_color_mixer.rst:24
msgid "You have on the left side the current color."
msgstr "Ліворуч показано поточний колір."

#: ../../reference_manual/dockers/digital_color_mixer.rst:26
msgid ""
"Next to that there are six columns. Each of these columns consists of three "
"rows: The lowest row is the color that you are mixing the current color "
"with. Ticking this button allows you to set a different color using a "
"palette and the mini-color wheel. The slider above this mixing color "
"represent the proportions of the mixing color and the current color. The "
"higher the slider, the less of the mixing color will be used in mixing. "
"Finally, the result color. Clicking this will change your current color to "
"the result color."
msgstr ""
"Поряд розташовано шість стовпчиків. У кожному з цих стовпчиків три рядки. У "
"найнижчому рядку вказано колір, з яким ви змішуєте поточний колір. "
"Натискання кнопки надасть вам змогу вказати інший колір за допомогою палітри "
"та мініатюрного кола кольорів. За допомогою повзунка, розташованого над поле "
"вибору кольору можна встановити співвідношення між кольором змішування та "
"поточним кольором. Чим більшим буде значення на повзунку, тим менше кольору "
"змішування буде використано. І нарешті, верхнім є рядок кольору результату. "
"Натискання цього рядка надасть змогу замінити поточний колір на результат "
"змішування."
