# Translation of docs_krita_org_reference_manual___tools___multibrush.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___multibrush\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 19:43+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:40
msgid ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"
msgstr ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"

#: ../../reference_manual/tools/multibrush.rst:None
msgid ".. image:: images/tools/Krita-multibrush.png"
msgstr ".. image:: images/tools/Krita-multibrush.png"

#: ../../reference_manual/tools/multibrush.rst:1
msgid "Krita's multibrush tool reference."
msgstr "Довідник із інструмента мультипензля Krita."

#: ../../reference_manual/tools/multibrush.rst:11
#: ../../reference_manual/tools/multibrush.rst:31
msgid "Symmetry"
msgstr "Відбити симетрично"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Multibrush"
msgstr "Мультипензель"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Mandala"
msgstr "Мандала"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Rotational Symmetry"
msgstr "Обертальна симетрія"

#: ../../reference_manual/tools/multibrush.rst:16
msgid "Multibrush Tool"
msgstr "Інструмент «Мультипензель»"

#: ../../reference_manual/tools/multibrush.rst:18
msgid "|toolmultibrush|"
msgstr "|toolmultibrush|"

#: ../../reference_manual/tools/multibrush.rst:20
msgid ""
"The Multibrush tool allows you to draw using multiple instances of a "
"freehand brush stroke at once, it can be accessed from the Toolbox docker or "
"with the default shortcut :kbd:`Q`. Using the Multibrush is similar to "
"toggling the :ref:`mirror_tools`, but the Multibrush is more sophisticated, "
"for example it can mirror freehand brush strokes along a rotated axis."
msgstr ""
"Інструмент «Мультипензель» уможливлює малювання за допомогою декількох "
"екземплярів пензля довільного малювання одним штрихом. Доступ до інструмента "
"можна отримати за допомогою бічної панелі інструментів або типового "
"клавіатурного скорочення :kbd:`Q`. Використання мультипензля подібне до "
"вмикання інструмента :ref:`mirror_tools`, але мультипензель є складнішим "
"інструментом; наприклад, у ньому передбачено можливість віддзеркалення "
"мазків пензлем довільного малювання щодо не вертикальної і не горизонтальної "
"вісі."

#: ../../reference_manual/tools/multibrush.rst:22
msgid "The settings for the tool will be found in the tool options dock."
msgstr ""
"Параметри цього інструмента розташовано на бічній панелі параметрів "
"інструмента."

#: ../../reference_manual/tools/multibrush.rst:24
msgid ""
"The multibrush tool has three modes and the settings for each can be found "
"in the tool options dock. Symmetry and mirror reflect over an axis which can "
"be set in the tool options dock. The default axis is the center of the "
"canvas."
msgstr ""
"У інструмента мультипензля передбачено три режими, а параметри для кожного з "
"них розташовано на бічній панелі параметрів інструмента. Симетрія і "
"віддзеркалення відбивають мазки пензля відносно вісі, параметри якої можна "
"встановити на бічній панелі параметрів інструмента. Типово, віддзеркалення "
"відбувається відносно центру полотна."

#: ../../reference_manual/tools/multibrush.rst:29
msgid "The three modes are:"
msgstr "Три режими:"

#: ../../reference_manual/tools/multibrush.rst:32
msgid ""
"Symmetry will reflect your brush around the axis at even intervals. The "
"slider determines the number of instances which will be drawn on the canvas."
msgstr ""
"Симетрія віддзеркалить ваш пензель щодо вісі з правильними інтервалами. За "
"допомогою повзунка визначається кількість екземплярів мазків, які буде "
"намальовано на полотні."

#: ../../reference_manual/tools/multibrush.rst:33
msgid "Mirror"
msgstr "Віддзеркалити"

#: ../../reference_manual/tools/multibrush.rst:34
msgid "Mirror will reflect the brush across the X axis, the Y axis, or both."
msgstr ""
"Віддзеркалення виконає відбиття пензля відносно вісі X, вісі Y або обох осей."

#: ../../reference_manual/tools/multibrush.rst:35
msgid "Translate"
msgstr "Пересунути"

#: ../../reference_manual/tools/multibrush.rst:36
msgid ""
"Translate will paint the set number of instances around the cursor at the "
"radius distance."
msgstr ""
"Пересування намалює встановлену кількість екземплярів навколо курсора у "
"межах відстані, яка визначається радіусом."

#: ../../reference_manual/tools/multibrush.rst:37
msgid "Snowflake"
msgstr "Сніжинка"

#: ../../reference_manual/tools/multibrush.rst:38
msgid ""
"This works as a mirrored symmetry, but is a bit slower than symmetry+toolbar "
"mirror mode."
msgstr ""
"Цей варіант працює як дзеркальна симетрія, але є трохи повільнішим за "
"симетрію і режим віддзеркалення з панелі інструментів."

#: ../../reference_manual/tools/multibrush.rst:40
msgid "Copy Translate"
msgstr "Копіювальне перенесення"

#: ../../reference_manual/tools/multibrush.rst:40
msgid ""
"This allows you to set the position of the copies relative to your own "
"cursor. To set the position of the copies, first toggle :guilabel:`Add`, and "
"then |mouseleft| the canvas to place copies relative to the multibrush "
"origin. Finally, press :guilabel:`Add` again, and start drawing to see the "
"copy translate in action."
msgstr ""
"За допомогою цього пункту можна встановити позицію копій відносно курсора. "
"Щоб визначити позицію копій, спочатку натисніть кнопку :guilabel:`Додати`, "
"потім клацніть |mouseleft| на полотні, щоб розташувати копії відносно "
"початкової позиції мультипензля. Нарешті, натисніть кнопку :guilabel:"
"`Додати` ще раз і почніть малювати, щоб побачити дію копіювального "
"перенесення."

#: ../../reference_manual/tools/multibrush.rst:42
msgid ""
"The assistant and smoothing options work the same as in the :ref:"
"`freehand_brush_tool`, though only on the real brush and not its copies."
msgstr ""
"Параметри допоміжних засобів та згладжування працюють так само, як "
"інструмент :ref:`freehand_brush_tool`, хоча лише для справжнього пензля, а "
"не для його копій."
