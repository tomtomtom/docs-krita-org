# Translation of docs_krita_org_tutorials___krita-brush-tips___animated_brushes.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-31 16:40+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:None
msgid ""
".. image:: images/brush-tips/Krita-animtedbrush.png\n"
"   :alt: krita Animated brush tip layer setup"
msgstr ""
".. image:: images/brush-tips/Krita-animtedbrush.png\n"
"   :alt: Ajustament de la capa de la punta del pinzell animat del Krita."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:None
msgid ""
".. image:: images/brush-tips/Krita-animtedbrush1.png\n"
"   :alt: Animated brush tips isolated layers"
msgstr ""
".. image:: images/brush-tips/Krita-animtedbrush1.png\n"
"   :alt: Capes aïllades per a les puntes del pinzell animat."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:None
msgid ""
".. image:: images/brush-tips/Krita-animtedbrush2.png\n"
"   :alt: Predefined brush tips dialog"
msgstr ""
".. image:: images/brush-tips/Krita-animtedbrush2.png\n"
"   :alt: Diàleg per a les puntes del pinzell predefinit."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:None
msgid ""
".. image:: images/brush-tips/Krita-animtedbrush3.png\n"
"   :alt: Animated brush image dialog"
msgstr ""
".. image:: images/brush-tips/Krita-animtedbrush3.png\n"
"   :alt: Diàleg per a la imatge del pinzell animat."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:None
msgid ""
".. image:: images/brush-tips/Krita-animtedbrush4.png\n"
"   :alt: Result of an animated brush"
msgstr ""
".. image:: images/brush-tips/Krita-animtedbrush4.png\n"
"   :alt: Resultat d'un pinzell animat."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:1
msgid "A brief explanation about animated brushes and how to use them"
msgstr "Una breu explicació sobre els pinzells animats i com emprar-los"

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:15
msgid "Brush-tips:Animated Brushes"
msgstr "Puntes de pinzell: pinzells animats"

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:18
msgid "Question"
msgstr "Pregunta"

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:20
msgid ""
"I was messing with the brushes and noticed there is like an option for it "
"being \"animated\". What does it mean and how do I use it?"
msgstr ""
"Estava jugant amb els pinzells i m'he adonat que hi ha una opció perquè "
"siguin «animats». Què vol dir i com emprar-la?"

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:22
msgid ""
"Basically, they’re what is officially called an ‘image hose’, and they’re "
"quite fun. They are basically a brush-tip with multiple image files."
msgstr ""
"Bàsicament, és el que oficialment es coneix com a «mànega d'imatge», i és "
"força divertit. Bàsicament són una punta de pinzell amb múltiples fitxers "
"d'imatge."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:24
msgid ""
"The typical way to make them is to first draw the ‘frames’ on a small "
"canvas, per layer:"
msgstr ""
"La forma típica de crear-los és dibuixar primer els «fotogrames» en un llenç "
"petit, un per capa:"

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:30
msgid ""
"You can use the :kbd:`Alt +` |mouseleft| shortcut on the layer thumbnails to "
"isolate layers without hiding them."
msgstr ""
"Podeu utilitzar la drecera :kbd:`Alt + feu` |mouseleft| sobre les miniatures "
"de les capes per aïllar-les sense ocultar-les."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:35
msgid "When done you should have a mess like this."
msgstr "Quan acabeu, hauríeu de tenir un embolic com aquest."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:37
msgid ""
"Go into the brush settings (:kbd:`F5` key), and go to predefined brush-tips, "
"and click stamp. You will get this window."
msgstr ""
"Aneu als ajustaments per al pinzell (tecla :kbd:`F5`), aneu a les puntes del "
"pinzell predefinit i feu clic a Segell. Obtindreu aquesta finestra."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:42
msgid "And then use style **animated** and selection mode set to **random**."
msgstr ""
"I després utilitzeu l'estil **animat** i el mode de selecció establert a "
"**aleatori**."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:44
msgid ""
"Krita uses Gimp’s image hose format which allows for random selection of the "
"images, angle based selection, pressure based selection, and incremental "
"selection (I have no idea what constant does)."
msgstr ""
"El Krita utilitza el format de mànega d'imatge del Gimp, el qual permet la "
"selecció aleatòria de les imatges, la selecció basada en l'angle, la "
"selecció basada en la pressió i la selecció incremental (no tinc idea del "
"que fa la constant)."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:49
msgid "Then select the above brush and your new leafy-brush tip."
msgstr ""
"Després seleccioneu el pinzell anterior i la vostra frondosa punta de "
"pinzell nova."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:54
msgid "And use it to paint trees! (for example)"
msgstr "I empreu-la per a pintar arbres! (Per exemple)."

#: ../../tutorials/krita-brush-tips/animated_brushes.rst:56
msgid ""
"You can also use animated brush tips to emulate, for example, bristle brush "
"tips that go from very fine bristles to a fully opaque stamp based on "
"pressure, like a dry paintbrush might do. To do this, you would follow the "
"above instructions, but for each layer, create a different cross-section of "
"the brush to correspond with the amount of pressure applied."
msgstr ""
"També podreu utilitzar les puntes del pinzell animat per emular, per "
"exemple, les puntes del pinzell de pèl, les quals van des de pèls molt fins "
"a un segell completament opac basat en la pressió, com ho faria un pinzell "
"de pintura en sec. Per a fer això, haureu de seguir les instruccions "
"anteriors, però per a cada capa, creeu una secció transversal diferent del "
"pinzell perquè es correspongui amb la quantitat de pressió aplicada."
