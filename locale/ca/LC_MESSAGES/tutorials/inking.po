# Translation of docs_krita_org_tutorials___inking.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-31 16:55+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_fingers.gif\n"
"   :alt: finger movement"
msgstr ""
".. image:: images/inking/Stroke_fingers.gif\n"
"   :alt: Moviment dels dits."

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_wrist.gif\n"
"   :alt: wrist movement"
msgstr ""
".. image:: images/inking/Stroke_wrist.gif\n"
"   :alt: Moviment del canell."

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_arm.gif\n"
"   :alt: arm movement"
msgstr ""
".. image:: images/inking/Stroke_arm.gif\n"
"   :alt: Moviment del braç."

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_shoulder.gif\n"
"   :alt: stroke shoulder movement"
msgstr ""
".. image:: images/inking/Stroke_shoulder.gif\n"
"   :alt: Moviment de l'espatlla durant el traç."

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Inking_aliasresize.png\n"
"   :alt: aliased resize"
msgstr ""
".. image:: images/inking/Inking_aliasresize.png\n"
"   :alt: Canviant la mida amb pixelat."

#: ../../tutorials/inking.rst:1
msgid "tips and tricks for inking in Krita"
msgstr "Consells i trucs per aplicar tinta en el Krita"

#: ../../tutorials/inking.rst:13
msgid "Inking"
msgstr "Aplicar tinta"

#: ../../tutorials/inking.rst:15
msgid ""
"The first thing to realize about inking is that unlike anatomy, perspective, "
"composition or color theory, you cannot compensate for lack of practice with "
"study or reasoning. This is because all the magic in drawing lines happens "
"from your shoulder to your fingers, very little of it happens in your head, "
"and your lines improve with practice."
msgstr ""
"El primer que haureu de tenir en compte sobre aplicar tinta és que, a "
"diferència de l'anatomia, la perspectiva, la composició o la teoria del "
"color, no pot compensar la falta de pràctica amb l'estudi o el raonament. "
"Això es deu al fet que tota la màgia en el traçat de les línies ocorre des "
"de l'espatlla fins als dits, molt poc succeeix al cap, i les línies milloren "
"amb la pràctica."

#: ../../tutorials/inking.rst:17
msgid ""
"On the other hand, this can be a blessing. You don’t need to worry about "
"whether you are smart enough, or are creative enough to be a good inker. "
"Just dedicated. Doubtlessly, inking is the Hufflepuff of drawing disciplines."
msgstr ""
"D'altra banda, això pot ser una benedicció. No cal preocupar-se si sou prou "
"intel·ligent o creatiu com per a ser un bon dibuixant en tinta. Només cal "
"ser dedicat. Sens dubte, l'aplicar tinta és el més laboriós entre les "
"disciplines de dibuix."

#: ../../tutorials/inking.rst:19
msgid "That said, there are a few tips to make life easy:"
msgstr "Dit això, hi ha alguns consells per a fer la vida més fàcil:"

#: ../../tutorials/inking.rst:22
msgid "Pose"
msgstr "Posat"

#: ../../tutorials/inking.rst:24
msgid ""
"Notice how I mentioned up there that the magic happens between your "
"shoulders and fingers? A bit weird, not? But perhaps, you have heard of "
"people talking about adopting a different pose for drawing."
msgstr ""
"Tingueu en compte que com he esmentat anteriorment, la màgia succeeix entre "
"les espatlles i els dits? Una mica estrany, no? Però potser, hàgiu sentit a "
"parlar de persones que adopten un posat diferent per a dibuixar."

#: ../../tutorials/inking.rst:26
msgid ""
"You can in fact, make different strokes depending on which muscles and "
"joints you use to make the movement: The Fingers, the wrist and lower-arm "
"muscles, the elbow and upper-arm muscles or the shoulder and back muscles."
msgstr ""
"De fet, podreu realitzar diferents traços segons els músculs i les "
"articulacions que feu servir per a fer el moviment: els músculs dels dits, "
"del canell i la part inferior del braç, els músculs del colze i la part "
"superior del braç o els músculs de l'espatlla i l'esquena."

#: ../../tutorials/inking.rst:34
msgid ""
"Generally, the lower down the arm the easier it is to make precise strokes, "
"but also the less durable the joints are for long term use. We tend to start "
"off using our fingers and wrist a lot during drawing, because it’s easier to "
"be precise this way. But it’s difficult to make long strokes, and "
"furthermore, your fingers and wrist get tired far quicker."
msgstr ""
"En general, com més baixa estigui la part inferior del braç, més fàcil serà "
"fer moviments precisos, però també les articulacions es cansen més durant un "
"ús prolongat. Durant el dibuix tendim a fer servir molt els dits i el "
"canell, perquè és més fàcil ser precís d'aquesta manera. Però és difícil fer "
"traços llargs i, a més, els dits i el canell es cansen molt més ràpid."

#: ../../tutorials/inking.rst:42
msgid ""
"Your shoulders and elbows on the other hand are actually quite good at "
"handling stress, and if you use your whole hand you will be able to make "
"long strokes far more easily. People who do calligraphy need shoulder based "
"strokes to make those lovely flourishes (personally, I can recommend "
"improving your handwriting as a way to improve inking), and train their arms "
"so they can do both big and small strokes with the full arm."
msgstr ""
"D'altra banda, les vostres espatlles i colzes són prou bons per a manejar "
"l'estrès, i si feu servir tota la vostra mà podreu realitzar traçades "
"llargues amb molta més facilitat. Les persones que fan cal·ligrafia "
"necessiten traçades basades en l'espatlla per a fer aquestes precioses flors "
"(personalment, puc recomanar-vos millorar la vostra escriptura a mà com a "
"una forma de millorar l'aplicació de la tinta), i entrenar els vostres "
"braços perquè puguin fer traços grans i petits amb tot el braç."

# skip-rule: kct-remove
#: ../../tutorials/inking.rst:44
msgid ""
"To control pressure in this state effectively, you should press your pinky "
"against the tablet surface as you make your stroke. This will allow you to "
"precisely judge how far the pen is removed from the tablet surface while "
"leaving the position up to your shoulders. The pressure should then be put "
"by your elbow."
msgstr ""
"Per a controlar la pressió de manera efectiva en aquest estat, haureu de "
"prémer el vostre dit petit contra la superfície de la tauleta mentre "
"realitzeu el traç. Això permetrà jutjar amb precisió a on es retirarà el "
"llapis de la superfície de la tauleta mentre deixeu la posició a l'alçada de "
"les espatlles. La pressió s'haurà d'aplicar amb el colze."

#: ../../tutorials/inking.rst:46
msgid ""
"So, there are not any secret rules to inking, but if there is one, it would "
"be the following: *The longer your stroke, the more of your arms you need to "
"use to make the stroke*."
msgstr ""
"Per tant, no hi ha cap regla secreta per aplicar la tinta, però si n'hi ha "
"una, seria la següent: *com més llarg sigui el vostre traç, més podreu fer "
"sevir els braços per a realitzar el traç*."

#: ../../tutorials/inking.rst:49
msgid "Stroke smoothing"
msgstr "Suavitzat del traç"

# skip-rule: kct-cut
#: ../../tutorials/inking.rst:51
msgid ""
"So, if the above is the secret to drawing long strokes, that would be why "
"people having been inking lovely drawings for years without any smoothing? "
"Then, surely, it is decadence to use something like stroke smoothing, a "
"short-cut for the lazy?"
msgstr ""
"Aleshores, si l'anterior és el secret per a dibuixar traços llargs. És per "
"això que la gent porta anys aplicant tinta en bonics dibuixos sense cap "
"tipus de suavitzat? Llavors, segurament, és decadent utilitzar quelcom com "
"el suavitzat del traç. Una drecera per als mandrosos?"

#: ../../tutorials/inking.rst:56
msgid ""
".. image:: images/inking/Stroke_rigger.gif\n"
"   :alt: rigger brush demonstration"
msgstr ""
".. image:: images/inking/Stroke_rigger.gif\n"
"   :alt: Demostració del pinzell amb truges."

#: ../../tutorials/inking.rst:56
msgid ""
"Example of how a rigger brush can smooth the original movement (here in red)"
msgstr ""
"Exemple de com un pinzell amb truges pot suavitzar el moviment original "
"(aquí en vermell)."

#: ../../tutorials/inking.rst:58
msgid ""
"Not really. To both, actually. Inkers have had a real-life tool that made it "
"easier to ink, it’s called a rigger-brush, which is a brush with very long "
"hairs. Due to this length it sorta smooths out shakiness, and thus a "
"favoured brush when inking at three in the morning."
msgstr ""
"Realment no. En realitat, per a ambdós. Els que dibuixen en tinta han tingut "
"una eina de la vida real que facilita l'entintat, anomenada pinzell amb "
"truges, la qual és un pinzell amb pèls molt llargs. A causa d'aquesta "
"longitud, se suavitza el tremolor i, per tant, és un pinzell adequat quan "
"s'aplica tinta a qualsevol hora del dia."

#: ../../tutorials/inking.rst:60
msgid ""
"With some tablet brands, the position events being sent aren’t very precise, "
"which is why we having basic smoothing to apply the tiniest bit of smoothing "
"on tablet strokes."
msgstr ""
"Amb algunes marques de tauletes, els esdeveniments de posició que s'envien "
"no són molt precisos, per això tenim un suavitzat bàsic per aplicar el més "
"mínim suavitzat en els traços des de la tauleta."

#: ../../tutorials/inking.rst:62
msgid ""
"On the other hand, doing too much smoothing during the whole drawing can "
"make your strokes very mechanical in the worst way. Having no jitter or tiny "
"bumps removes certain humanity from your drawings, and it can make it "
"impossible to represent fabric properly."
msgstr ""
"D'altra banda, suavitzar massa durant tot el dibuix pot fer que els vostres "
"traços siguin molt mecànics en la pitjor manera. No tenir Jitter o petites "
"irregularitats eliminarà de certa humanitat els vostres dibuixos, i farà que "
"sigui impossible representar correctament el teixit."

#: ../../tutorials/inking.rst:64
msgid ""
"Therefore, it’s wise to train your inking hand, yet not to be too hard on "
"yourself and refuse to use smoothing at all, as we all get tired, cold or "
"have a bad day once in a while. Stabilizer set to 50 or so should provide a "
"little comfort while keeping the little irregularities."
msgstr ""
"Per tant, és aconsellable entrenar la vostra mà a aplicar tinta, però no "
"sigueu massa dur i negueu-vos a utilitzar el suavitzat, ja que tothom es "
"cansa, de tant en tant tenim fred o un mal dia. Amb l'estabilitzador "
"establert a 50 o menys s'hauria de proporcionar una mica de comoditat mentre "
"es mantenen les petites irregularitats."

#: ../../tutorials/inking.rst:67
msgid "Bezier curves and other tools"
msgstr "Corbes de Bézier i altres eines"

#: ../../tutorials/inking.rst:69
msgid ""
"So, you may have heard of a French curve. If not, it’s a piece of plastic "
"representing a stencil. These curves are used to make perfectly smooth "
"curves on the basis of a sketch."
msgstr ""
"Llavors, possiblement heu sentit a parlar d'una corba francesa. Si no és "
"així, és una peça de plàstic que representa una plantilla. Aquestes corbes "
"s'utilitzen per a crear corbes perfectament suaus sobre la base d'un esbós."

#: ../../tutorials/inking.rst:71
msgid ""
"In digital painting, we don’t have the luxury of being able to use two "
"hands, so you can’t hold a ruler with one hand and adjust it while inking "
"with the other. For this purpose, we have instead Bezier curves, which can "
"be made with the :ref:`path_selection_tool`."
msgstr ""
"A la pintura digital, no tenim el luxe de poder utilitzar dues mans, de "
"manera que no podrem sostenir un regle amb una mà i ajustar-lo mentre "
"apliqueu tinta amb l'altra. Per a aquest propòsit, tenim les corbes de "
"Bézier, les quals es poden crear amb l':ref:`path_selection_tool`."

#: ../../tutorials/inking.rst:73
msgid ""
"You can even make these on a vector layer, so they can be modified on the "
"fly."
msgstr ""
"Fins i tot podreu crear-les en una capa vectorial, de manera que es podran "
"modificar sobre la marxa."

#: ../../tutorials/inking.rst:75
msgid ""
"The downside of these is that they cannot have line-variation, making them a "
"bit robotic."
msgstr ""
"El desavantatge d'això és que no poden tenir variacions de línia, el qual "
"les fa una mica robòtiques."

#: ../../tutorials/inking.rst:77
msgid ""
"You can also make small bezier curves with the :ref:`assistant_tool`, "
"amongst the other tools there."
msgstr ""
"També podreu crear petites corbes de Bézier amb l':ref:`assistant_tool`, "
"entre altres eines."

#: ../../tutorials/inking.rst:79
msgid ""
"Then, in the freehand brush tool options, you can tick :guilabel:`Snap to "
"Assistants` and start a line that snaps to this assistant."
msgstr ""
"Després, en les Opcions de l'eina per al pinzell a mà alçada, podreu marcar :"
"guilabel:`Ajusta als assistents` i iniciar una línia que s'ajusti a aquest "
"assistent."

#: ../../tutorials/inking.rst:82
msgid "Presets"
msgstr "Predefinits"

#: ../../tutorials/inking.rst:84
msgid ""
"So here are some things to consider with the brush-presets that you use:"
msgstr ""
"Aquí hi ha algunes coses a tenir en compte amb els pinzells predefinits que "
"utilitzareu:"

#: ../../tutorials/inking.rst:87
msgid "Anti-aliasing versus jagged pixels"
msgstr "Antialiàsing front el gra dels píxels"

#: ../../tutorials/inking.rst:89
msgid ""
"A starting inker might be inclined to always want to use anti-aliased "
"brushes, after all, they look so smooth on the screen. However, while these "
"look good on screen, they might become fuzzy when printing them. Therefore, "
"Krita comes with two default types. Anti-aliased brushes like ink_brush_25 "
"and slightly aliased brushes like ink_tilt, with the latter giving better "
"print results. If you are trying to prepare for both, it might be an idea to "
"consider making the inking page 600dpi and the color page 300dpi, so that "
"the inking page has a higher resolution and the ‘jaggies’ aren’t as visible. "
"You can turn any pixel brush into an aliased brush, by going the :kbd:`F5` "
"key and ticking **Sharpness**."
msgstr ""
"Un dibuixant en tinta principiant podria inclinar-se a voler utilitzar "
"sempre els pinzells amb antialiàsing, després de tot, es veuen tan suaus a "
"la pantalla. No obstant això, tot i que aquests es veuen bé a la pantalla, "
"poden aparèixer borrosos quan s'imprimeix. Per tant, el Krita ve amb dos "
"tipus predeterminats. Pinzells amb antialiàsing com «Ink_brush_25» i "
"pinzells lleugerament amb pixelat com l'«Ink_tilt», amb l'últim obtindreu "
"millors resultats en la impressió. Si proveu de preparar-vos per a ambdós, "
"una idea podria ser el considerar crear la pàgina per aplicar tinta a 600 "
"ppp i la pàgina de color a 300 ppp, de manera que la pàgina per aplicar "
"tinta tingui una resolució més gran i les «irregularitats» no siguin tan "
"visibles. Podreu convertir qualsevol pinzell de píxels en un pinzell amb "
"pixelat, prement la tecla :kbd:`F5` i marcant **Agudesa**."

#: ../../tutorials/inking.rst:92
msgid "Texture"
msgstr "Textura"

#: ../../tutorials/inking.rst:94
msgid ""
"Do you make smooth ‘wet’ strokes? Or do you make textured ones? For the "
"longest time, smooth strokes were preferred, as that would be less of a "
"headache when entering the coloring phase. Within Krita there are several "
"methods to color these easily, the colorize mask being the prime example, so "
"textured becomes a viable option even for the lazy amongst us."
msgstr ""
"Creeu traços suaus «humits»? O els creeu amb textura? Per als temps més "
"llargs, es prefereixen traços suaus, ja que això serà menys mal de cap quan "
"s'entri a la fase de donar color. Dins del Krita hi ha diversos mètodes per "
"a donar color amb facilitat, la màscara d'acoloriment és el millor exemple, "
"de manera que la textura esdevindrà en una opció viable fins i tot per als "
"que siguem mandrosos."

#: ../../tutorials/inking.rst:99
msgid ""
".. image:: images/inking/Inking_patterned.png\n"
"   :alt: type of strokes"
msgstr ""
".. image:: images/inking/Inking_patterned.png\n"
"   :alt: Tipus de traços."

#: ../../tutorials/inking.rst:99
msgid "Left: No texture, Center: Textured, Right: Predefined Brush tip"
msgstr ""
"Esquerra: Sense textura. Centre: Amb textura. Dreta: Punta del pinzell "
"predefinit."

#: ../../tutorials/inking.rst:102
msgid "Pressure curve"
msgstr "Corba de pressió"

#: ../../tutorials/inking.rst:104
msgid ""
"Of course, the nicest lines are made with pressure sensitivity, so they "
"dynamically change from thick to thin. However, different types of curves on "
"the pressure give different results. The typical example is a slightly "
"concave line to create a brush that more easily makes thin lines."
msgstr ""
"Per descomptat, les línies més boniques estaran creades amb la sensibilitat "
"a la pressió, de manera que canviaran dinàmicament de gruixudes a primes. No "
"obstant això, diferents tipus de corbes en la pressió donaran resultats "
"diferents. L'exemple típic és una línia lleugerament còncava per a crear un "
"pinzell que crei les línies fines amb més facilitat."

#: ../../tutorials/inking.rst:109
msgid ""
".. image:: images/inking/Ink_gpen.png\n"
"   :alt: pressure curve for ink gpen"
msgstr ""
".. image:: images/inking/Ink_gpen.png\n"
"   :alt: Corba de pressió per al pinzell Ink Gpen."

#: ../../tutorials/inking.rst:109
msgid ""
"Ink_Gpen_25 is a good example of a brush with a concave pressure curve. This "
"curve makes it easier to make thin lines."
msgstr ""
"L'«Ink_Gpen_25» és un bon exemple d'un pinzell amb una corba de pressió "
"còncava. Aquesta corba farà que sigui més fàcil crear línies fines."

#: ../../tutorials/inking.rst:114
msgid ""
".. image:: images/inking/Ink_convex.png\n"
"   :alt: convex inking brush"
msgstr ""
".. image:: images/inking/Ink_convex.png\n"
"   :alt: Pinzell de tinta convex."

#: ../../tutorials/inking.rst:114
msgid "conversely, here's a convex brush. The strokes are much rounder"
msgstr ""
"Per contra, aquí hi ha un pinzell convex. Els traços són molt més rodons."

#: ../../tutorials/inking.rst:119
msgid ""
".. image:: images/inking/Ink_fill_circle.png\n"
"   :alt: ink fill circle"
msgstr ""
".. image:: images/inking/Ink_fill_circle.png\n"
"   :alt: El pinzell «Fill_circle» de tinta."

#: ../../tutorials/inking.rst:119
msgid ""
"Fill_circle combines both into an s-curve, this allows for very dynamic "
"brush strokes"
msgstr ""
"El «Fill_circle» combina ambdós en una corba S, això permet pinzellades molt "
"dinàmiques."

#: ../../tutorials/inking.rst:124
msgid ""
".. image:: images/inking/Ink_speed.png\n"
"   :alt: inverse convex to speed parameter"
msgstr ""
".. image:: images/inking/Ink_speed.png\n"
"   :alt: Corba convexa inversa al paràmetre de la velocitat."

#: ../../tutorials/inking.rst:124
msgid ""
"Pressure isn't the only thing you can do interesting things with, adding an "
"inverse convex curve to speed can add a nice touch to your strokes"
msgstr ""
"La pressió no és l'única amb la que podreu fer coses interessants, afegint "
"una corba convexa inversa a la velocitat afegireu un toc agradable als "
"vostres traços."

#: ../../tutorials/inking.rst:127
msgid "Preparing sketches for inking"
msgstr "Preparar esbossos per aplicar tinta"

#: ../../tutorials/inking.rst:129
msgid ""
"So, you have a sketch and you wish to start inking it. Assuming you’ve "
"scanned it in, or drew it, you can try the following things to make it "
"easier to ink."
msgstr ""
"Llavors, teniu un esbós i voleu començar a aplicar-li tinta. Suposant que "
"l'heu escanejat o dibuixat, podreu intentar el següent per a facilitar el "
"fet d'aplicar la tinta."

#: ../../tutorials/inking.rst:132
msgid "Opacity down to 10%"
msgstr "Baixeu l'opacitat al 10%"

#: ../../tutorials/inking.rst:134
msgid ""
"Put a white (just press the :kbd:`Backspace` key) layer underneath the "
"sketch. Turn down the opacity of the sketch to a really low number and put a "
"layer above it for inking."
msgstr ""
"Poseu una capa blanca (simplement premeu la tecla :kbd:`Retrocés`) a sota de "
"l'esbós. Feu baixar l'opacitat de l'esbós a un nombre realment baix i poseu "
"una capa a sobre per aplicar la tinta."

#: ../../tutorials/inking.rst:137
msgid "Make the sketch colored"
msgstr "Creeu l'esbós en color"

#: ../../tutorials/inking.rst:139
msgid ""
"Put a layer filled with a color you like between the inking and sketch "
"layer. Then set that layer to ‘screen’ or ‘addition’, this will turn all the "
"black lines into the color! If you have a transparent background, or put "
"this layer into a group, be sure to tick the alpha-inherit symbol!"
msgstr ""
"Poseu una capa emplenada amb un color que us agradi entre la capa de tinta i "
"l'esbós. Després establiu aquesta capa a «Pantalla» o «Addició», això "
"convertirà totes les línies negres en color! Si teniu un fons transparent, o "
"col·loqueu aquesta capa en un grup, assegureu-vos de marcar el símbol Hereta "
"l'alfa!"

#: ../../tutorials/inking.rst:142
msgid "Make the sketch colored, alternative version"
msgstr "Creeu l'esbós en color, versió alternativa"

#: ../../tutorials/inking.rst:144
msgid ""
"Or, right-click the layer, go to layer properties, and untick ‘blue’. This "
"works easier with a single layer sketch, while the above works best with "
"multi-layer sketches."
msgstr ""
"O bé, feu |mouseright| sobre la capa, aneu a les propietats de la capa i "
"desmarqueu «Blau». Això funciona més fàcilment amb un esbós d'una sola capa, "
"mentre que l'anterior funciona millor amb esbossos de múltiples capes."

# skip-rule: k-Super
#: ../../tutorials/inking.rst:147
msgid "Super-thin lines"
msgstr "Línies molt primes"

#: ../../tutorials/inking.rst:149
msgid ""
"If you are interested in super-thin lines, it might be better to make your "
"ink at double or even triple the size you usually work at, and, only use an "
"aliased pixel brush. Then, when the ink is finished, use the fill tool to "
"fill in flats on a separate layer, split the layer via :menuselection:`Layer "
"--> Split --> Layer Split`, and then resize to the original size."
msgstr ""
"Si esteu interessat en línies súper primes, podria ser millor crear la "
"vostra tinta al doble o fins i tot triplicar la mida en la que generalment "
"treballeu, i només utilitzeu un pinzell de píxels amb pixelat. Després, quan "
"hàgiu acabat amb la tinta, utilitzeu l'eina d'emplenat per emplenar plans en "
"una capa separada, divideixi la capa a través de :menuselection:`Capa --> "
"Divideix --> Divideix la capa` i després canvieu la mida a la mida original."

#: ../../tutorials/inking.rst:154
msgid ""
"This might be a little of an odd way of working, but it does make drawing "
"thin lines trivial, and it's cheaper to buy RAM so you can make HUGE images "
"than to spent hours on trying to color the thin lines precisely, especially "
"as colorize mask will not be able to deal with thin anti-aliased lines very "
"well."
msgstr ""
"Aquesta podria ser una manera una mica estranya de treballar, però fa que "
"dibuixar línies primes sigui trivial, i és més barat que comprar RAM perquè "
"pugueu crear imatges ENORMES i no passeu hores intentant pintar línies "
"primes amb precisió, en especial en la màscara d'acoloriment no bregareu "
"gaire bé amb les línies primes amb antialiàsing."

# skip-rule: t-acc_obe
#: ../../tutorials/inking.rst:157
msgid ""
"David Revoy made a set of his own inking tips for Krita and explains them in "
"this `youtube video <https://www.youtube.com/watch?v=xvQ5l0edsq4>`_."
msgstr ""
"En David Revoy va crear un conjunt dels seus propis consells per aplicar "
"tinta en el Krita i els explica en aquest `vídeo de YouTube <https://www."
"youtube.com/watch?v=xvQ5l0edsq4>`_."
