# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-08-22 18:52+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita_3_0_1_Brush_engine_ratio.png"
msgstr ".. image:: images/brushes/Krita_3_0_1_Brush_engine_ratio.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:1
msgid "Krita's Brush Engine options overview."
msgstr "Översikt av alternativen i Kritas penselgränssnitt."

#: ../../reference_manual/brushes/brush_settings/options.rst:18
msgid "Options"
msgstr "Alternativ"

#: ../../reference_manual/brushes/brush_settings/options.rst:20
#: ../../reference_manual/brushes/brush_settings/options.rst:24
msgid "Airbrush"
msgstr "Retuschspruta"

#: ../../reference_manual/brushes/brush_settings/options.rst:27
msgid ".. image:: images/brushes/Krita_2_9_brushengine_airbrush.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_airbrush.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:28
msgid ""
"If you hold the brush still, but are still pressing down, this will keep "
"adding color onto the canvas. The lower the rate, the quicker the color gets "
"added."
msgstr ""
"Om man håller penseln stilla, och fortfarande trycker ner, fortsätter det "
"lägga till färg på duken. Ju lägre hastighet, ju snabbare läggs färgen till."

#: ../../reference_manual/brushes/brush_settings/options.rst:30
#: ../../reference_manual/brushes/brush_settings/options.rst:34
msgid "Mirror"
msgstr "Spegla"

#: ../../reference_manual/brushes/brush_settings/options.rst:37
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Mirror.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Mirror.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:38
msgid "This allows you to mirror the Brush-tip with Sensors."
msgstr "Gör det möjligt att spegla penselspetsen med sensorer."

#: ../../reference_manual/brushes/brush_settings/options.rst:40
msgid "Horizontal"
msgstr "Horisontell"

#: ../../reference_manual/brushes/brush_settings/options.rst:41
msgid "Mirrors the mask horizontally."
msgstr "Speglar masken horisontellt."

#: ../../reference_manual/brushes/brush_settings/options.rst:43
msgid "Vertical"
msgstr "Vertikal"

#: ../../reference_manual/brushes/brush_settings/options.rst:43
msgid "Mirrors the mask vertically."
msgstr "Speglar masken vertikalt."

#: ../../reference_manual/brushes/brush_settings/options.rst:46
msgid ".. image:: images/brushes/Krita_2_9_brushengine_mirror.jpg"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_mirror.jpg"

#: ../../reference_manual/brushes/brush_settings/options.rst:47
msgid ""
"Some examples of mirroring and using it in combination with :ref:"
"`option_rotation`."
msgstr ""
"Några exempel på spegling och använda det i kombination med :ref:"
"`option_rotation`."

#: ../../reference_manual/brushes/brush_settings/options.rst:52
msgid "Rotation"
msgstr "Rotation"

#: ../../reference_manual/brushes/brush_settings/options.rst:54
msgid "This allows you to affect Angle of your brush-tip with Sensors."
msgstr "Gör det möjligt att påverka penselspetsens vinkel med sensorer."

#: ../../reference_manual/brushes/brush_settings/options.rst:57
msgid ".. image:: images/brushes/Krita_2_9_brushengine_rotation.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_rotation.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:59
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Rotation.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Rotation.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:60
msgid "In the above example, several applications of the parameter."
msgstr "I exemplet ovan, flera användningar av parametern."

#: ../../reference_manual/brushes/brush_settings/options.rst:62
msgid ""
"Drawing Angle -- A common one, usually used in combination with rake-type "
"brushes. Especially effect because it does not rely on tablet-specific "
"sensors. Sometimes, Tilt-Direction or Rotation is used to achieve a similar-"
"more tablet focused effect, where with Tilt the 0° is at 12 o'clock, Drawing "
"angle uses 3 o'clock as 0°."
msgstr ""
"Ritvinkel: Vanlig, och ofta använd i kombination med krattliknande penslar. "
"Särskilt effektiv eftersom den inte beror på sensorer specifika för "
"ritplattor. Ibland används lutningsriktning eller rotation för att "
"åstadkomma en effekt mer fokuserad på ritplattor, där 0° är klockan tolv med "
"lutning. Ritvinkel använder klockan tre som 0°."

#: ../../reference_manual/brushes/brush_settings/options.rst:63
msgid ""
"Fuzzy -- Also very common, this gives a nice bit of randomness for texture."
msgstr ""
"Suddig: Också mycket vanlig, och ger en fin slumpmässighet till strukturer."

#: ../../reference_manual/brushes/brush_settings/options.rst:64
msgid ""
"Distance -- With careful editing of the Sensor curve, you can create nice "
"patterns."
msgstr ""
"Avstånd: Med försiktig redigering av sensorkurvan kan man skapa fina mönster."

#: ../../reference_manual/brushes/brush_settings/options.rst:65
msgid "Fade -- This slowly fades the rotation from one into another."
msgstr "Tona: Tonar sakta rotationen från en till en annan."

#: ../../reference_manual/brushes/brush_settings/options.rst:66
msgid ""
"Pressure -- An interesting one that can create an alternative looking line."
msgstr ""
"Tryck: En intressant parameter som kan skapa en linje med alternativt "
"utseende."

#: ../../reference_manual/brushes/brush_settings/options.rst:71
msgid "Scatter"
msgstr "Spridning"

#: ../../reference_manual/brushes/brush_settings/options.rst:73
msgid ""
"This parameter allows you to set the random placing of a brush-dab. You can "
"affect them with Sensors."
msgstr ""
"Parametern gör det möjligt att ställa in slumpmässig placering av "
"penselklick. De kan påverkas av sensorer."

#: ../../reference_manual/brushes/brush_settings/options.rst:75
msgid "X"
msgstr "X"

#: ../../reference_manual/brushes/brush_settings/options.rst:76
msgid "The scattering on the angle you are drawing from."
msgstr "Spridningen för vinkeln som man ritar med."

#: ../../reference_manual/brushes/brush_settings/options.rst:78
msgid "Y"
msgstr "Y"

#: ../../reference_manual/brushes/brush_settings/options.rst:78
msgid ""
"The scattering, perpendicular to the drawing angle (has the most effect)."
msgstr "Spridningen vinkelrät mot ritvinkeln (har största effekten)."

#: ../../reference_manual/brushes/brush_settings/options.rst:81
msgid ".. image:: images/brushes/Krita_2_9_brushengine_scatter.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_scatter.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:85
msgid "Sharpness"
msgstr "Skärpa"

#: ../../reference_manual/brushes/brush_settings/options.rst:88
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Sharpness.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Sharpness.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:89
msgid ""
"Puts a threshold filter over the brush mask. This can be used for brush like "
"strokes, but it also makes for good pixel art brushes."
msgstr ""
"Lägger till ett tröskelfilter över penselmasken. Det kan användas för "
"penselliknande streck, men leder också till bra penslar för bildpunktskonst."

#: ../../reference_manual/brushes/brush_settings/options.rst:91
msgid "Strength"
msgstr "Styrka"

#: ../../reference_manual/brushes/brush_settings/options.rst:92
msgid "Controls the threshold, and can be controlled by the sensors below."
msgstr "Bestämmer tröskeln och kan kontrolleras av sensorerna nedan."

#: ../../reference_manual/brushes/brush_settings/options.rst:94
#: ../../reference_manual/brushes/brush_settings/options.rst:120
msgid "Softness"
msgstr "Mjukhet"

#: ../../reference_manual/brushes/brush_settings/options.rst:94
msgid ""
"Controls the extra non-fully opaque pixels. This adds a little softness to "
"the stroke."
msgstr ""
"Bestämmer de extra inte helt ogenomskinliga bildpunkter. Det lägger till en "
"viss mjukhet till strecket."

#: ../../reference_manual/brushes/brush_settings/options.rst:98
msgid ""
"The sensors now control the threshold instead of the subpixel precision, "
"softness slider was added."
msgstr ""
"Sensorerna bestämmer nu tröskeln istället för precisionen för "
"delbildpunkterna, och mjukhetsreglaget har lagts till."

#: ../../reference_manual/brushes/brush_settings/options.rst:103
msgid "Size"
msgstr "Storlek"

#: ../../reference_manual/brushes/brush_settings/options.rst:106
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Size.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Size.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:107
msgid ""
"This parameter is not the diameter itself, but rather the curve for how it's "
"affected."
msgstr ""
"Parametern är inte själva diametern, utan istället kurvan för hur den "
"påverkas."

#: ../../reference_manual/brushes/brush_settings/options.rst:109
msgid ""
"So, if you want to lock the diameter of the brush, lock the Brush-tip. "
"Locking the size parameter will only lock this curve. Allowing this curve to "
"be affected by the Sensors can be very useful to get the right kind of "
"brush. For example, if you have trouble drawing fine lines, try to use a "
"concave curve set to pressure. That way you'll have to press hard for thick "
"lines."
msgstr ""
"Så om man vill låsa penselns diameter, lås då penselspetsen. Att låsa "
"storleksparametern låser bara kurvan. Att tillåta att kurvan påverkas av "
"sensorerna kan vara mycket användbart för att få rätt sorts pensel. Om man "
"exempelvis har problem med att rita tunna linjer, kan man försöka använda en "
"konkav kurva inställd till tryck. På så sätt behöver man inte trycka hårt "
"för att få tjocka linjer."

#: ../../reference_manual/brushes/brush_settings/options.rst:112
msgid ".. image:: images/brushes/Krita_2_9_brushengine_size_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_size_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:113
msgid ""
"Also popular is setting the size to the sensor fuzzy or perspective, with "
"the later in combination with a :ref:`assistant_perspective`."
msgstr ""
"Det är också populärt att ställa in storleken till sensorerna suddig eller "
"perspektiv, där den senare används i kombination med en :ref:"
"`assistant_perspective`."

#: ../../reference_manual/brushes/brush_settings/options.rst:116
msgid ".. image:: images/brushes/Krita_2_9_brushengine_size_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_size_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:122
msgid "This allows you to affect Fade with Sensors."
msgstr "Gör det möjligt att påverka toning med sensorer."

#: ../../reference_manual/brushes/brush_settings/options.rst:125
msgid ".. image:: images/brushes/Krita_2_9_brushengine_softness.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_softness.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:126
msgid ""
"Has a slight brush-decreasing effect, especially noticeable with soft-brush, "
"and is overall more noticeable on large brushes."
msgstr ""
"Har en något minskande penseleffekt, i synnerhet märkbar med mjuka penslar, "
"och generellt mer märkbar för stora penslar."

#: ../../reference_manual/brushes/brush_settings/options.rst:131
msgid "Source"
msgstr "Källa"

#: ../../reference_manual/brushes/brush_settings/options.rst:133
msgid "Picks the source-color for the brush-dab."
msgstr "Väljer källfärgen för penselklicket."

#: ../../reference_manual/brushes/brush_settings/options.rst:135
msgid "Plain Color"
msgstr "Enkel färg"

#: ../../reference_manual/brushes/brush_settings/options.rst:136
msgid "Current foreground color."
msgstr "Aktuell förgrundsfärg."

#: ../../reference_manual/brushes/brush_settings/options.rst:137
#: ../../reference_manual/brushes/brush_settings/options.rst:169
msgid "Gradient"
msgstr "Toning"

#: ../../reference_manual/brushes/brush_settings/options.rst:138
msgid "Picks active gradient."
msgstr "Väljer aktiv toning."

#: ../../reference_manual/brushes/brush_settings/options.rst:139
msgid "Uniform Random"
msgstr "Likfördelat slumpmässigt"

#: ../../reference_manual/brushes/brush_settings/options.rst:140
msgid "Gives a random color to each brush dab."
msgstr "Ger en slumpmässig färg för varje penselklick."

#: ../../reference_manual/brushes/brush_settings/options.rst:141
msgid "Total Random"
msgstr "Fullständigt slumpmässigt"

#: ../../reference_manual/brushes/brush_settings/options.rst:142
msgid "Random noise pattern is now painted."
msgstr "Slumpmässigt brusmönster målas nu."

#: ../../reference_manual/brushes/brush_settings/options.rst:143
msgid "Pattern"
msgstr "Mönster"

#: ../../reference_manual/brushes/brush_settings/options.rst:144
msgid "Uses active pattern, but alignment is different per stroke."
msgstr "Använder aktivt fönster, men justeringen är olika för varje streck."

#: ../../reference_manual/brushes/brush_settings/options.rst:146
msgid "Locked Pattern"
msgstr "Låst mönster"

#: ../../reference_manual/brushes/brush_settings/options.rst:146
msgid "Locks the pattern to the brushdab."
msgstr "Låser mönstret till penselklicket."

#: ../../reference_manual/brushes/brush_settings/options.rst:151
msgid "Mix"
msgstr "Blanda"

#: ../../reference_manual/brushes/brush_settings/options.rst:153
msgid ""
"Allows you to affect the mix of the :ref:`option_source` color with Sensors. "
"It will work with Plain Color and Gradient as source. If Plain Color is "
"selected as source, it will mix between foreground and background colors "
"selected in color picker. If Gradient is selected, it chooses a point on the "
"gradient to use as painting color according to the sensors selected."
msgstr ""
"Gör det möjligt att påverka blandningen av :ref:`option_source` färg med "
"sensorer. Det fungerar med Ren färg och Toning som källa. Om Ren färg väljes "
"som källa, blandas den mellan förgrunds- och bakgrundsfärgerna som är valda "
"i färghämtaren. Om Toning används, väljes en punkt i toningen att använda "
"som målarfärg enligt valda sensorer."

#: ../../reference_manual/brushes/brush_settings/options.rst:156
msgid ".. image:: images/brushes/Krita_2_9_brushengine_mix_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_mix_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:158
msgid "Uses"
msgstr "Använder"

#: ../../reference_manual/brushes/brush_settings/options.rst:161
msgid ".. image:: images/brushes/Krita_2_9_brushengine_mix_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_mix_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:163
msgid ""
"The above example uses a :program:`Krita` painted flowmap in the 3D program :"
"program:`Blender`. A brush was set to :menuselection:`Source --> Gradient` "
"and :menuselection:`Mix --> Drawing angle`. The gradient in question "
"contained the 360° for normal map colors. Flow maps are used in several "
"Shaders, such as brushed metal, hair and certain river-shaders."
msgstr ""
"Exemplet ovan använder en målad flödesavbildning från :program:`Krita`i "
"tredimensionsprogrammet :program:`Blender`. Penseln är inställd till :"
"menuselection:`Källa --> Toning` och :menuselection:`Blanda --> Ritvinkel`. "
"Den aktuella toningen innehåller 360° normalavbildningsfärger. "
"Flödesavbildningar används i flera skuggningar, såsom borstad metall, hår "
"och vissa flodskuggningar."

#: ../../reference_manual/brushes/brush_settings/options.rst:164
msgid "Flow map"
msgstr "Flödesavbildning"

#: ../../reference_manual/brushes/brush_settings/options.rst:171
msgid ""
"Exactly the same as using :menuselection:`Source --> Gradient` with :"
"guilabel:`Mix`, but only available for the Color Smudge Brush."
msgstr ""
"Exakt samma sak som att använda :menuselection:`Källa --> Toning` med :"
"guilabel:`Blanda`, men bara tillgängligt för färgsmetningspenseln."

#: ../../reference_manual/brushes/brush_settings/options.rst:173
#: ../../reference_manual/brushes/brush_settings/options.rst:177
msgid "Spacing"
msgstr "Mellanrum"

#: ../../reference_manual/brushes/brush_settings/options.rst:180
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Spacing.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Spacing.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:181
msgid "This allows you to affect :ref:`option_brush_tip` with :ref:`sensors`."
msgstr ""
"Gör det möjligt att påverka :ref:`option_brush_tip` med :ref:`sensors`."

#: ../../reference_manual/brushes/brush_settings/options.rst:184
msgid ".. image:: images/brushes/Krita_2_9_brushengine_spacing_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_spacing_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:186
msgid "Isotropic spacing"
msgstr "Isotropiskt mellanrum"

#: ../../reference_manual/brushes/brush_settings/options.rst:186
msgid ""
"Instead of the spacing being related to the ratio of the brush, it will be "
"on diameter only."
msgstr ""
"Istället för att mellanrummet är relaterat till penselns förhållande, gäller "
"den bara diametern."

#: ../../reference_manual/brushes/brush_settings/options.rst:189
msgid ".. image:: images/brushes/Krita_2_9_brushengine_spacing_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_spacing_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:190
#: ../../reference_manual/brushes/brush_settings/options.rst:194
msgid "Ratio"
msgstr "Förhållande"

#: ../../reference_manual/brushes/brush_settings/options.rst:196
msgid ""
"Allows you to change the ratio of the brush and bind it to parameters. This "
"also works for predefined brushes."
msgstr ""
"Gör det möjligt att ändra penselns förhållande och koppla det till "
"parametrar. Det fungerar också på fördefinierade penslar."
