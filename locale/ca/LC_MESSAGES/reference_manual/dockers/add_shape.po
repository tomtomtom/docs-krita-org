# Translation of docs_krita_org_reference_manual___dockers___add_shape.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-19 17:20+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/dockers/add_shape.rst:1
msgid "The add shape docker."
msgstr "L'acoblador Afegeix una forma."

#: ../../reference_manual/dockers/add_shape.rst:15
msgid "Add Shape"
msgstr "Afegeix una forma"

#: ../../reference_manual/dockers/add_shape.rst:18
msgid ".. image:: images/dockers/Krita_Add_Shape_Docker.png"
msgstr ".. image:: images/dockers/Krita_Add_Shape_Docker.png"

#: ../../reference_manual/dockers/add_shape.rst:19
msgid "A docker for adding KOffice shapes to a Vector Layers."
msgstr "Un acoblador per afegir formes del KOffice a les Capes vectorials."

#: ../../reference_manual/dockers/add_shape.rst:23
msgid "This got removed in 4.0, the :ref:`vector_library_docker` replacing it."
msgstr ""
"Ha estat eliminat a la versió 4.0: l'ha substituït la :ref:"
"`vector_library_docker`."
