# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:18+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en KritaArtisticColorSelectorDocker icons image\n"
"X-POFile-SpellExtra: Kritamouseleft Kritamouseright images alt mouseright\n"
"X-POFile-SpellExtra: mouseleft dockers colormodels ref gamutmaskdocker\n"
"X-POFile-SpellExtra: Luma Westerflier HSI pretobranco\n"
"X-POFile-SpellExtra: artisticcolorselectordockerselectorsettings Krita\n"
"X-POFile-SpellExtra: guilabel artisticcolorselectordockerwheelpreferences\n"
"X-POFile-SpellExtra: tot Gamute Hövell Wolthera linearandgamma Gurney van\n"
"X-POFile-SpellExtra: HSY\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/dockers/artistic_color_selector.rst:1
msgid "Overview of the artistic color selector docker."
msgstr "Introdução à área do selector de cores artísticas."

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Artistic Color Selector"
msgstr "Selecção de Cores Artística"

#: ../../reference_manual/dockers/artistic_color_selector.rst:17
msgid "Artistic Color Selector Docker"
msgstr "Área do Selector de Cores Artísticas"

#: ../../reference_manual/dockers/artistic_color_selector.rst:19
msgid "A color selector inspired by traditional color wheel and workflows."
msgstr ""
"Um selector de cores inspirado na roda de cores tradicional e nos seus "
"procedimentos associados."

#: ../../reference_manual/dockers/artistic_color_selector.rst:22
msgid "Usage"
msgstr "Utilização"

#: ../../reference_manual/dockers/artistic_color_selector.rst:26
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:26
msgid "Artistic color selector with a gamut mask."
msgstr "Área do selector de cores artísticas com uma máscara de gamute."

#: ../../reference_manual/dockers/artistic_color_selector.rst:28
msgid ""
"Select hue and saturation on the wheel (5) and value on the value scale (4). "
"|mouseleft| changes foreground color (6). |mouseright| changes background "
"color (7)."
msgstr ""
"Seleccione a matiz e a saturação na roda (5) e o valor na escala de valores "
"(4). O |mouseleft| muda a cor principal (6). O |mouseright| muda a cor de "
"fundo (7)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:30
msgid ""
"The blip shows the position of current foreground color on the wheel "
"(black&white circle) and on the value scale (black&white line). Last "
"selected swatches are outlined."
msgstr ""
"O traço mostra a posição da cor principal actual na roda (círculo "
"preto&branco) e na escala de valores (linha a preto&branco). As últimas "
"amostras seleccionadas estão destacadas."

#: ../../reference_manual/dockers/artistic_color_selector.rst:32
msgid ""
"Parameters of the wheel can be set in :ref:"
"`artistic_color_selector_docker_wheel_preferences` menu (2). Selector "
"settings are found under :ref:"
"`artistic_color_selector_docker_selector_settings` menu (3)."
msgstr ""
"Os parâmetros da roda podem ser definidos no menu :ref:"
"`artistic_color_selector_docker_wheel_preferences` (2). As definições do "
"selector podem ser encontradas no menu :ref:"
"`artistic_color_selector_docker_selector_settings` (3)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:35
msgid "Gamut Masking"
msgstr "Máscara do Gamute"

#: ../../reference_manual/dockers/artistic_color_selector.rst:37
msgid ""
"You can select and manage your gamut masks in the :ref:`gamut_mask_docker`."
msgstr ""
"Poderá seleccionar e gerir as suas máscaras de gamute no :ref:"
"`gamut_mask_docker`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:39
msgid ""
"In the gamut masking toolbar (1) you can toggle the selected mask off and on "
"(left button). You can also rotate the mask with the rotation slider (right)."
msgstr ""
"Na barra de máscara do gamute (1) poderá activar/desactivar a máscara "
"seleccionada (botão esquerdo). Poderá também rodar a máscara com a barra de "
"rotação (direita)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:45
msgid "Color wheel preferences"
msgstr "Preferências da roda de cores"

#: ../../reference_manual/dockers/artistic_color_selector.rst:50
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_3.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_3.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:50
msgid "Color wheel preferences."
msgstr "Preferências da roda de cores."

#: ../../reference_manual/dockers/artistic_color_selector.rst:53
msgid "Sliders 1, 2, and 3"
msgstr "Barras 1, 2 e 3"

#: ../../reference_manual/dockers/artistic_color_selector.rst:53
msgid ""
"Adjust the number of steps of the value scale, number of hue sectors and "
"saturation rings on the wheel, respectively."
msgstr ""
"Ajusta o número de passos na escala de valores, o número de sectores de "
"matiz e os anéis de saturação na roda, respectivamente."

#: ../../reference_manual/dockers/artistic_color_selector.rst:56
msgid "Continuous Mode"
msgstr "Modo Contínuo"

#: ../../reference_manual/dockers/artistic_color_selector.rst:56
msgid ""
"The value scale and hue sectors can also be set to continuous mode (with the "
"infinity icon on the right of the slider). If toggled on, the respective "
"area shows a continuous gradient instead of the discrete swatches."
msgstr ""
"A escala de valores e os sectores de matiz também podem ser configurados com "
"o modo contínuo (com o ícone de infinito à direita da barra). Se estiver "
"activado, a área respectiva mostra um gradiente contínuo em vez das amostras "
"discretas."

#: ../../reference_manual/dockers/artistic_color_selector.rst:59
msgid "Invert saturation (4)"
msgstr "Inverter a saturação (4)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:59
msgid ""
"Changes the order of saturation rings within the the hue sectors. By "
"default, the wheel has gray in the center and most saturated colors on the "
"perimeter. :guilabel:`Invert saturation` puts gray on the perimeter and most "
"saturated colors in the center."
msgstr ""
"muda a ordem dos anéis de saturação dentro dos sectores de matiz. Por "
"omissão, a roda tem o cinzento no centro e as cores mais saturadas no "
"perímetro. A opção :guilabel:`Inverter a saturação` coloca o cinzento no "
"perímetro e as cores mais saturadas no centro."

#: ../../reference_manual/dockers/artistic_color_selector.rst:62
msgid ""
"Loads default values for the sliders 1, 2, and 3. These default values are "
"configured in selector settings."
msgstr ""
"Carrega os valores predefinidos para as barras 1, 2 e 3. Estes valores "
"predefinidos estão configurados na configuração do selector."

#: ../../reference_manual/dockers/artistic_color_selector.rst:63
msgid "Reset to default (5)"
msgstr "Repor a predefinição (5)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:68
msgid "Selector settings"
msgstr "Configuração do selector"

#: ../../reference_manual/dockers/artistic_color_selector.rst:72
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_2.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:72
msgid "Selector settings menu."
msgstr "Menu de configuração do selector."

#: ../../reference_manual/dockers/artistic_color_selector.rst:75
msgid "Show background color indicator"
msgstr "Mostrar uma indicação da cor de fundo"

#: ../../reference_manual/dockers/artistic_color_selector.rst:76
msgid "Toggles the bottom-right triangle with current background color."
msgstr "Comuta o triângulo inferior-direito com a cor de fundo actual."

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid "Selector Appearance (1)"
msgstr "Aparência do Selector (1)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid "Show numbered value scale"
msgstr "Mostrar uma escala numérica de valores"

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid ""
"If checked, the value scale includes a comparative gray scale with lightness "
"percentage."
msgstr ""
"Se estiver assinalado, a escala de valores inclui uma escala de tons de "
"cinzento comparativa com a percentagem de luminosidade."

#: ../../reference_manual/dockers/artistic_color_selector.rst:81
msgid ""
"Set the color model used by the selector. For detailed information on color "
"models, see :ref:`color_models`."
msgstr ""
"Define o modelo de cores usado pelo selector. Para informações mais "
"detalhadas sobre os modelos de cores, veja em :ref:`color_models`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid "Color Space (2)"
msgstr "Espaço de Cores (2)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid "Luma Coefficients (3)"
msgstr "Coeficientes de Luma (3)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid ""
"If the selector's color space is HSY, you can set custom Luma coefficients "
"and the amount of gamma correction applied to the value scale (set to 1.0 "
"for linear scale; see :ref:`linear_and_gamma`)."
msgstr ""
"Se o espaço de cores do selector é o HSY, poderá definir coeficientes de "
"Luma personalizados e a quantidade de correcção do gama aplicada à escala de "
"valores (configure como 1,0 para uma escala linear; veja o :ref:"
"`linear_and_gamma`)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:87
msgid "Gamut Masking Behavior (4)"
msgstr "Comportamento da Máscara de Gamute (4)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:87
msgid ""
"The selector can be set either to :guilabel:`Enforce gamut mask`, so that "
"colors outside the mask cannot be selected, or to :guilabel:`Just show the "
"shapes`, where the mask is visible but color selection is not limited."
msgstr ""
"O selector poderá ser configurado como :guilabel:`Forçar a máscara do "
"gamute`, para que as cores fora da máscara não possam ser seleccionadas, ou "
"como :guilabel:`Apenas mostrar as formas`, onde a máscara fica visível mas a "
"selecção de cores não fica limitada."

#: ../../reference_manual/dockers/artistic_color_selector.rst:90
msgid "Default Selector Steps Settings"
msgstr "Configuração dos Passos do Selector Predefinido"

#: ../../reference_manual/dockers/artistic_color_selector.rst:90
msgid ""
"Values the color wheel and value scale will be reset to default when the :"
"guilabel:`Reset to default` button in :ref:"
"`artistic_color_selector_docker_wheel_preferences` is pressed."
msgstr ""
"Os valores na roda de cores e na escala de valores serão repostos com as "
"predefinições quando carregar no botão :guilabel:`Repor as predefinições` "
"em :ref:`artistic_color_selector_docker_wheel_preferences`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:93
msgid "External Info"
msgstr "Informação Externa"

#: ../../reference_manual/dockers/artistic_color_selector.rst:94
msgid ""
"`HSI and HSY for Krita’s advanced colour selector by Wolthera van Hövell tot "
"Westerflier. <https://wolthera.info/?p=726>`_"
msgstr ""
"O `HSI e o HSY para o selector de cores avançado do Krita por Wolthera van "
"Hövell tot Westerflier. <http://wolthera.info/?p=726>`_"

#: ../../reference_manual/dockers/artistic_color_selector.rst:95
msgid ""
"`The Color Wheel, Part 7 by James Gurney. <https://gurneyjourney.blogspot."
"com/2010/02/color-wheel-part-7.html>`_"
msgstr ""
"`A Roda de Cores, Parte 7 de James Gurney. <https://gurneyjourney.blogspot."
"com/2010/02/color-wheel-part-7.html>`_"
