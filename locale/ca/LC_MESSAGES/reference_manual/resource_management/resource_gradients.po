# Translation of docs_krita_org_reference_manual___resource_management___resource_gradients.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-08-24 16:53+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

# skip-rule: punctuation-period
#: ../../<generated>:1
msgid "HSV counter-clock wise."
msgstr "HSV en sentit antihorari"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_new_gradient.png"
msgstr ".. image:: images/gradients/Krita_new_gradient.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_stop_gradient.png"
msgstr ".. image:: images/gradients/Krita_stop_gradient.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_move_stop.png"
msgstr ".. image:: images/gradients/Krita_move_stop.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_stop_sudden_change.png"
msgstr ".. image:: images/gradients/Krita_stop_sudden_change.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_segment_gradient_options.png"
msgstr ".. image:: images/gradients/Krita_segment_gradient_options.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_gradient_segment_blending.png"
msgstr ".. image:: images/gradients/Krita_gradient_segment_blending.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_gradient_hsv_cw.png"
msgstr ".. image:: images/gradients/Krita_gradient_hsv_cw.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:1
msgid "Creating and managing gradients in Krita."
msgstr "Crear i gestionar els degradats al Krita."

#: ../../reference_manual/resource_management/resource_gradients.rst:11
#: ../../reference_manual/resource_management/resource_gradients.rst:16
msgid "Gradients"
msgstr "Degradats"

#: ../../reference_manual/resource_management/resource_gradients.rst:11
msgid "Resources"
msgstr "Recursos"

#: ../../reference_manual/resource_management/resource_gradients.rst:19
msgid "Accessing a Gradient"
msgstr "Accedir a un degradat"

#: ../../reference_manual/resource_management/resource_gradients.rst:21
msgid ""
"The Gradients configuration panel is accessed by clicking the Gradients icon "
"(usually the icon next to the disk)."
msgstr ""
"Al plafó de configuració per als degradats s'accedeix fent clic a la icona "
"Degradats (normalment la icona situada al costat del disc)."

#: ../../reference_manual/resource_management/resource_gradients.rst:24
msgid ".. image:: images/gradients/Gradient_Toolbar_Panel.png"
msgstr ".. image:: images/gradients/Gradient_Toolbar_Panel.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:25
msgid ""
"Gradients are configurations of blending between colors.  Krita provides "
"over a dozen preset dynamic gradients for you to choose from.  In addition, "
"you can design and save your own."
msgstr ""
"Els degradats són configuracions de barreja entre colors. El Krita "
"proporciona més d'una dotzena de degradats dinàmics predefinits per a que "
"trieu. A més, podreu dissenyar i desar els vostres."

#: ../../reference_manual/resource_management/resource_gradients.rst:27
msgid "Some typical uses of gradients are:"
msgstr "Alguns usos típics dels degradats són:"

#: ../../reference_manual/resource_management/resource_gradients.rst:29
msgid "Fill for vector shapes."
msgstr "Emplenar per a les formes vectorials."

#: ../../reference_manual/resource_management/resource_gradients.rst:30
msgid "Gradient tool"
msgstr "Eina de degradat."

#: ../../reference_manual/resource_management/resource_gradients.rst:31
msgid "As a source of color for the pixel brush."
msgstr "Com a font de color per al pinzell de píxels."

#: ../../reference_manual/resource_management/resource_gradients.rst:33
msgid ""
"There is no gradients docker. They can only be accessed through the gradient "
"\"quick-menu\" in the toolbar."
msgstr ""
"No hi ha cap acoblador per als degradats. Només s'hi pot accedir a través "
"del «menú ràpid» Degradats que hi ha a la barra d'eines."

#: ../../reference_manual/resource_management/resource_gradients.rst:36
msgid "Editing a Gradient"
msgstr "Editar un degradat"

#: ../../reference_manual/resource_management/resource_gradients.rst:38
msgid "Krita has two gradient types:"
msgstr "El Krita té dos tipus de degradats:"

#: ../../reference_manual/resource_management/resource_gradients.rst:40
msgid ""
"Segmented Gradients, which are compatible with GIMP, have many different "
"features but are also a bit complicated to make."
msgstr ""
"Els degradats segmentats, els quals són compatibles amb el GIMP, tenen "
"moltes característiques diferents però també són una mica complicats de "
"crear."

#: ../../reference_manual/resource_management/resource_gradients.rst:41
msgid ""
"Stop Gradients, which are saved as SVG files and similar to how most "
"applications do their gradients, but has less features than the segmented "
"gradient."
msgstr ""
"Els degradats amb aturades, els quals es desen com a fitxers SVG i són "
"similars a la forma en què la majoria de les aplicacions fan els seus "
"degradats, però tenen menys característiques que el degradat segmentat."

#: ../../reference_manual/resource_management/resource_gradients.rst:43
msgid ""
"Initially we could only make segmented gradients in Krita, but in 3.0.2 we "
"can also make stop gradients."
msgstr ""
"Inicialment, al Krita només vàrem poder crear degradats segmentats, però des "
"de la versió 3.0.2 també podem crear degradats amb aturades."

#: ../../reference_manual/resource_management/resource_gradients.rst:48
msgid ""
"You can make a new gradient by going into the drop-down and selecting the "
"gradient type you wish to have. By default Krita will make a stop-gradient."
msgstr ""
"Podreu crear un degradat nou si aneu a la llista desplegable i seleccioneu "
"el tipus de degradat que voleu tenir. De manera predeterminada, el Krita "
"crearà un degradat amb aturades."

#: ../../reference_manual/resource_management/resource_gradients.rst:51
msgid "Stop Gradients"
msgstr "Degradats amb aturades"

#: ../../reference_manual/resource_management/resource_gradients.rst:56
msgid "Stop gradients are very straight forward:"
msgstr "Els degradats amb aturades són molt senzills:"

#: ../../reference_manual/resource_management/resource_gradients.rst:58
msgid "|mouseleft| on the gradient to add a stop."
msgstr "Feu |mouseleft| sobre el degradat per afegir una aturada."

#: ../../reference_manual/resource_management/resource_gradients.rst:59
msgid "|mouseleft| on the stops to select them, and drag to move them."
msgstr ""
"Feu |mouseleft| sobre les aturades per a seleccionar-les i arrossegueu per a "
"moure-les."

#: ../../reference_manual/resource_management/resource_gradients.rst:60
msgid ""
"|mouseright| on the stops to remove them. A stop gradient will not allow you "
"to remove stops if there's only two left."
msgstr ""
"Feu |mouseright| sobre les aturades per eliminar-les. Un degradat amb "
"aturades no permetrà eliminar les aturades si només en queden dues."

#: ../../reference_manual/resource_management/resource_gradients.rst:65
msgid ""
"A selected stop can have its color and transparency changed using the color "
"button and the opacity slider below."
msgstr ""
"A una aturada seleccionada se li pot canviar el color i la transparència "
"utilitzant el botó de color i el control lliscant d'opacitat."

#: ../../reference_manual/resource_management/resource_gradients.rst:70
msgid ""
"As per SVG spec, you can make a sudden change between stops by moving them "
"close together. The stops will overlap, but you can still drag them around."
msgstr ""
"D'acord amb l'especificació SVG, podreu fer un canvi sobtat entre les "
"aturades movent-les per estar juntes. Les aturades se superposaran, però "
"encara es podran arrossegar."

#: ../../reference_manual/resource_management/resource_gradients.rst:73
msgid "Segmented Gradients"
msgstr "Degradats segmentats"

#: ../../reference_manual/resource_management/resource_gradients.rst:76
msgid ".. image:: images/gradients/Krita_Editing_Custom_Gradient.png"
msgstr ".. image:: images/gradients/Krita_Editing_Custom_Gradient.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:77
msgid ""
"Segmented gradients are a bit more tricky. Instead of going from color to "
"color, it allows you to define segments, which each can have a begin and end "
"color."
msgstr ""
"Els degradats segmentats són una mica més difícils. En lloc d'anar d'un "
"color al color, permeten definir segments, cadascun dels quals podrà "
"contenir un color d'inici i final."

#: ../../reference_manual/resource_management/resource_gradients.rst:79
msgid "|mouseright| the gradient to call up this menu:"
msgstr "Feu |mouseright| sobre el degradat per a cridar a aquest menú:"

#: ../../reference_manual/resource_management/resource_gradients.rst:84
msgid "Split Segment"
msgstr "Divideix el segment"

#: ../../reference_manual/resource_management/resource_gradients.rst:85
msgid ""
"This splits the current segment in two, using the white arrow, the segment "
"middle as the place to split. It will also use the color at the white arrow "
"to define the new colors in place in the new segments."
msgstr ""
"Dividirà el segment actual en dos, utilitzant la fletxa blanca i el centre "
"del segment serà el lloc per a dividir. També utilitzarà el color a la "
"fletxa blanca per a definir els colors nous en el lloc dels segments nous."

#: ../../reference_manual/resource_management/resource_gradients.rst:86
msgid "Duplicate segment"
msgstr "Duplica el segment"

#: ../../reference_manual/resource_management/resource_gradients.rst:87
msgid ""
"Similar to split, but instead the two new segments are copies of the old one."
msgstr ""
"Similar a dividir, però en aquest cas els dos segments nous seran còpies de "
"l'anterior."

# skip-rule: punctuation-period
#: ../../reference_manual/resource_management/resource_gradients.rst:88
msgid "Mirror segment"
msgstr "Emmiralla el segment"

#: ../../reference_manual/resource_management/resource_gradients.rst:89
msgid "Mirrors the segment colors."
msgstr "Reflectirà els colors del segment."

#: ../../reference_manual/resource_management/resource_gradients.rst:91
msgid "Remove segment"
msgstr "Elimina el segment"

#: ../../reference_manual/resource_management/resource_gradients.rst:91
msgid "Removes the segment."
msgstr "Eliminarà el segment."

#: ../../reference_manual/resource_management/resource_gradients.rst:93
msgid ""
"|mouseleft| + dragging the black arrows will resize the segments attaching "
"to those arrows. |mouseleft| + dragging the white arrows will change the mid "
"point of that segment, changing the way how the mixture is made."
msgstr ""
":kbd:`Fer` |mouseleft| :kbd:`+ arrossega` sobre les fletxes negres canviarà "
"la mida dels segments que estan units amb aquestes fletxes. :kbd:`Fer` |"
"mouseleft| :kbd:`+ arrossega` sobre les fletxes blanques canviarà el punt "
"mig d'aquest segment, canviant la manera en què es farà la mescla."

#: ../../reference_manual/resource_management/resource_gradients.rst:95
msgid ""
"At the bottom, you can set the color and transparency of either part of the "
"segment."
msgstr ""
"A la part inferior, podreu establir el color i la transparència de qualsevol "
"de les parts del segment."

#: ../../reference_manual/resource_management/resource_gradients.rst:97
msgid "You can also set the blending. The first is the interpolation mode:"
msgstr "També podreu establir la barreja. La primera és el mode interpolació:"

#: ../../reference_manual/resource_management/resource_gradients.rst:102
msgid "Linear - Does a linear blending between both segments."
msgstr "Lineal: farà una barreja lineal entre els dos segments."

#: ../../reference_manual/resource_management/resource_gradients.rst:103
msgid "Curved - This causes the mix to ease-in and out faster."
msgstr "Corbada: farà que la mescla pugui créixer i disminuir més ràpidament."

#: ../../reference_manual/resource_management/resource_gradients.rst:104
msgid ""
"Sine - Uses a sine function. This causes the mix to ease in and out slower."
msgstr ""
"Sinusoïdal: utilitza una funció sinusoïdal. Farà que la mescla pugui créixer "
"i disminuir més lentament."

#: ../../reference_manual/resource_management/resource_gradients.rst:105
msgid ""
"Sphere, increasing - This puts emphasis on the later color during the mix."
msgstr ""
"Esfera, creixent: durant la barreja posarà l'èmfasi en el color posterior."

#: ../../reference_manual/resource_management/resource_gradients.rst:106
msgid ""
"Sphere, decreasing - This puts emphasis on the first color during the mix."
msgstr ""
"Esfera, decreixent: durant la barreja posarà l'èmfasi en el primer color."

#: ../../reference_manual/resource_management/resource_gradients.rst:108
msgid "Finally, there's the model:"
msgstr "Finalment, hi ha el model:"

#: ../../reference_manual/resource_management/resource_gradients.rst:113
msgid "RGB"
msgstr "RGB"

#: ../../reference_manual/resource_management/resource_gradients.rst:114
msgid "Does the blending in RGB model."
msgstr "Farà la barreja en el model RGB."

#: ../../reference_manual/resource_management/resource_gradients.rst:115
msgid "HSV clockwise"
msgstr "HSV en sentit horari"

#: ../../reference_manual/resource_management/resource_gradients.rst:116
msgid ""
"Blends the two colors using the HSV model, and follows the hue clockwise "
"(red-yellow-green-cyan-blue-purple). The above screenshot is an example of "
"this."
msgstr ""
"Barrejarà els dos colors utilitzant el model HSV i seguint el to en el "
"sentit de les agulles del rellotge (vermell-groc-verd-cian-blau-porpra). La "
"captura de pantalla anterior n'és un exemple."

#: ../../reference_manual/resource_management/resource_gradients.rst:118
msgid "Blends the color as the previous options, but then counter-clockwise."
msgstr ""
"Barrejarà el color com en l'opció anterior, però en sentit contrari a les "
"agulles del rellotge."
